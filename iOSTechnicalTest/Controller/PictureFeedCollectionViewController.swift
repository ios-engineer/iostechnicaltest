
import UIKit

private let reuseIdentifier = "Cell"

class PictureFeedCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout {

    var arrayResult : NSMutableArray!
    var shouldEndPage = Bool()
    var pageIndex : Int = 1
    
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 3,
        minimumLineSpacing: 3,
        sectionInset: UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView?.collectionViewLayout = columnLayout
        if #available(iOS 11.0, *) {
            collectionView?.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }
        self.getFlowerList(pageIndex: pageIndex)
    }
    
    // MARK: Seervice Call
    func getFlowerList(pageIndex : Int) {
        
        let urlString = "https://pixabay.com/api/?key=10961674-bf47eb00b05f514cdd08f6e11&q=flower&page=\(pageIndex)"
        let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: escapedString!) else{
            return
        }
        
        IndicatorHUD.shared.showHUD(message: "Please wait...")
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let _ = data,error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return
            }
            do {
                // data we are getting from network request
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let response = try decoder.decode(Flower.self, from: data!)
               
                self.pageIndex = pageIndex
                
                if pageIndex == 3{
                   self.shouldEndPage = true
                }
                
                if self.arrayResult == nil || self.arrayResult.count > 0 && pageIndex == 1{
                    self.arrayResult = NSMutableArray.init(array: response.hits)
                }else{
                   self.arrayResult.addObjects(from: response.hits)
                }
                
                DispatchQueue.main.async {
                    IndicatorHUD.shared.dismissGlobalHUD()
                    self.collectionView.reloadData()
                }
                
            } catch { print(error)
                DispatchQueue.main.async {
                    IndicatorHUD.shared.dismissGlobalHUD()
                }
            }
         }
        task.resume()
    }
}

// MARK: UICollectionViewDataSource
extension PictureFeedCollectionViewController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayResult?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.arrayResult.count > 0 && indexPath.row == self.arrayResult.count - 1 && self.shouldEndPage == false{
            self.getFlowerList(pageIndex: pageIndex + 1)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? PictureFeedCollectionCell else { return UICollectionViewCell()}
        if let model = self.arrayResult.object(at: indexPath.row) as? Hit{
            cell.feedImageView.loadImageUsingCache(withUrl: model.previewURL!)
        }
        return cell
    }
}

class PictureFeedCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var feedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
 
    func loadImageUsingCache(withUrl urlString : String) {
       
        guard let url = URL(string: urlString) else{
            return
        }
        self.image = nil
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = cachedImage
            return
        }
        
        let activity = UIActivityIndicatorView.init(frame: self.frame)
        activity.style = .gray
        activity.hidesWhenStopped = true
        activity.startAnimating()
        self.addSubview(activity)
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                    activity.stopAnimating()

                }
            }
            
        }).resume()
    }
}
