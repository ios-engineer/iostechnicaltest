
import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var pictureView: UIView!
    @IBOutlet weak var videoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pictureView.setViewCornerRadius(cornerRadius: pictureView.frame.height/2)
        videoView.setViewCornerRadius(cornerRadius: videoView.frame.height/2)
    }
}

