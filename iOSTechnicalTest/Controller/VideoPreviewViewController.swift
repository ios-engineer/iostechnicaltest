
import UIKit

class VideoPreviewViewController: UIViewController {

    @IBOutlet weak var objCustomPlayer: CustomPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name("StopVideo"), object: nil)
    }
    
    func startPlay() {
        objCustomPlayer.startPlayback()
    }
    
    func stopPlay() {
        objCustomPlayer.pausePlayback()
    }
}
