
import UIKit

class IndicatorHUD: NSObject {

    var hud: MBProgressHUD = MBProgressHUD()

    static let shared : IndicatorHUD = {
        let instance = IndicatorHUD()
        return instance
    }()
    
    
    func showHUD(message : String) {
       
        DispatchQueue.main.async {
          
            if self.hud.isHidden{
                self.hud.label.text = message
            }else{
                let view = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.view
                self.hud = MBProgressHUD.showAdded(to: view!, animated: true)
                self.hud.label.text = message
                self.hud.minSize = CGSize.init(width: 100, height: 100)
            }
        }
        
       
       
    }
    
    func showHUDWithProgress(percantage : Float) {
        
        hud.mode = .determinate
        hud.label.text = "Downloading..."
        hud.progress = percantage
        
    }
    
    
     func dismissGlobalHUD() -> Void{
        
        DispatchQueue.main.async {
            self.hud.hide(animated: true)
        }
      
    }
    
    
}
