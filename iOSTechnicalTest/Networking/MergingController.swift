
import UIKit
import AVFoundation
import AVKit

class MergingController: NSObject {

    static let shared : MergingController = {
         let instance = MergingController()
         return instance
    }()
    
    func mergeMutableVideoWithAudio(videoUrl:URL, audioUrl:URL) {
      
        let mixComposition : AVMutableComposition = AVMutableComposition()
        var mutableCompositionVideoTrack : [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack : [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction : AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        
        
        //start merge
        
        let aVideoAsset : AVAsset = AVAsset(url: videoUrl)
        let aAudioAsset : AVAsset = AVAsset(url: audioUrl)
        
        mutableCompositionVideoTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        mutableCompositionAudioTrack.append( mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        
        let aVideoAssetTrack : AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioAssetTrack : AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        
        
        do{
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)
            
            //In my case my audio file is longer then video file so i took videoAsset duration
            //instead of audioAsset duration
            
            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
            
            //Use this instead above line if your audiofile and video file's playing durations are same
            
            //            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(kCMTimeZero, aVideoAssetTrack.timeRange.duration), ofTrack: aAudioAssetTrack, atTime: kCMTimeZero)
            
        }catch{
            print("there was an ERROR MERGING AUDIO AND VIDEO ERROR ERROR ERROR")
        }
        
        totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero,duration: aVideoAssetTrack.timeRange.duration )
        
        let mutableVideoComposition : AVMutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
        mutableVideoComposition.renderSize = CGSize(width: 1280, height: 720)
        //        playerItem = AVPlayerItem(asset: mixComposition)
        //        player = AVPlayer(playerItem: playerItem!)
        //
        //
        //        AVPlayerVC.player = player
        
        //find your video on this URl
        let savePathUrl : URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/newVideo.mp4")
        
        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.outputURL = savePathUrl
        assetExport.shouldOptimizeForNetworkUse = true
        
        removeFileAtURLIfExists(url: savePathUrl)
        
        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {
                
            case AVAssetExportSession.Status.completed:
                
                //self.FullExportCompletion(assetExport)
                print("success")
                IndicatorHUD.shared.dismissGlobalHUD()
                NotificationCenter.default.post(name: Notification.Name("PlayVideo"), object: nil)
                
            case  AVAssetExportSession.Status.failed:
                print("failed \(assetExport.error!)")
            case AVAssetExportSession.Status.cancelled:
                print("cancelled \(assetExport.error!)")
            default:
                print("complete")
            }
        }
    }
    
    func removeFileAtURLIfExists(url: URL) {
        let filePath = url.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            do{
                try fileManager.removeItem(atPath: filePath)
            } catch let error as NSError {
                print("-----Couldn't remove existing destination file: \(error)")
            }
        }
        
    }
    
   
    func downloadAudioFile() {

        if let audioUrl = URL(string: "https://audio-ssl.itunes.apple.com/apple-assets-us-std-000001/AudioPreview122/v4/8a/dd/1f/8add1f4d-142c-1317-250d-ff6370962fb8/mzaf_7601694821840779604.plus.aac.p.m4a") {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                IndicatorHUD.shared.showHUD(message: "Merging video with audio...")
                self.mergeMutableVideoWithAudio(videoUrl: self.getVideoPath(), audioUrl: destinationUrl)
                // if the file doesn't exist
            } else {
                
                IndicatorHUD.shared.showHUD(message: "Prepaing...")
                // you can use NSURLSession.sharedSession to download the data asynchronously
                let task = DownloadManager.shared.activate().downloadTask(with: audioUrl)
                task.resume()
                
                DownloadManager.shared.onProgress = { (progress) in
                    OperationQueue.main.addOperation {
                        IndicatorHUD.shared.showHUDWithProgress(percantage: progress)
                    }
                }
            }
        }
        
    }
    
   
    
    
    func getVideoPath() -> URL {
       
        let bundlePath = Bundle.main.path(forResource: "Movie", ofType: "mp4")
        return URL.init(fileURLWithPath: bundlePath!)
        
    }
    
    
    func getSavedVideoPath(fileName : String) -> String {
        
        let filePath : URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/\(fileName)")
      
        let fileManager = FileManager.default
       
        if fileManager.fileExists(atPath: filePath.path) {
            return filePath.path
        }
        return ""
    }
    
    
}
