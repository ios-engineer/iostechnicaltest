
import UIKit
import AVFoundation
import AVKit

class CustomPlayer: UIView {

    fileprivate var player:AVPlayer?
    fileprivate var playerLayer:AVPlayerLayer?
    fileprivate var playerItem:AVPlayerItem?
    
    fileprivate var timer:Timer?
    fileprivate var playUrl:String!
    fileprivate var playbackObserver:Any?

     let playerBottomView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 1
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var playerSlider : UISlider = {
        let slider = UISlider()
        slider.tintColor = UIColor.clear
        slider.backgroundColor = UIColor.clear
        slider.maximumTrackTintColor = UIColor.white
        slider.minimumTrackTintColor = UIColor(red: 231/255, green: 107/255, blue: 107/255, alpha: 1)
        slider.minimumValue = 0
        slider.isContinuous = false
        slider.translatesAutoresizingMaskIntoConstraints = false
        return slider
    }()
    
    let timeLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 10)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00:00/00:00"
        return label
    }()
    
   lazy var playButton : UIButton = {
        let button = UIButton()
        button.setTitle("Start", for: .normal)
        button.setTitle("Pause", for: .selected)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.red
        button.layer.cornerRadius = 8
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(startPlay), for: .touchUpInside)
        return button
    }()
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    //MARK:- Interface Builder(Xib,StoryBoard)
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        setPlayerSubviewsFrame()
    }

    deinit {
        removeAllObserver()
        resettingObject()
    }
   
    private func setupViews(){
      self.superview?.addSubview(playerBottomView)
      playerBottomView.addSubview(playerSlider)
      playerBottomView.addSubview(timeLabel)
      self.superview?.addSubview(playButton)
      NotificationCenter.default.addObserver(self, selector: #selector(self.startPlay), name: Notification.Name("PlayVideo"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.stopVideo), name: Notification.Name("StopVideo"), object: nil)
    }
    
    private func setPlayerSubviewsFrame(){
          playerLayer?.frame = bounds
          playerBottomView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
          playerBottomView.topAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
          playerBottomView.widthAnchor.constraint(equalTo: self.widthAnchor, constant: 0).isActive = true
          playerBottomView.heightAnchor.constraint(equalToConstant: 50).isActive = true

          playerSlider.leftAnchor.constraint(equalTo: playerBottomView.leftAnchor, constant: 10).isActive = true
          playerSlider.centerYAnchor.constraint(equalTo: playerBottomView.centerYAnchor, constant: 0).isActive = true
          playerSlider.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
          timeLabel.leftAnchor.constraint(equalTo: playerSlider.rightAnchor, constant: 10).isActive =  true
          timeLabel.topAnchor.constraint(equalTo: playerSlider.topAnchor, constant: 0).isActive = true
          timeLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 20).isActive = true
          timeLabel.widthAnchor.constraint(equalToConstant: 110).isActive = true
          timeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
          playButton.centerXAnchor.constraint(equalTo: playerBottomView.centerXAnchor, constant: 0).isActive = true
          playButton.topAnchor.constraint(equalTo: playerBottomView.bottomAnchor, constant: 15).isActive = true
          playButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
          playButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    //MARK: - public control method
    public func playVideo(_ url:String) {
        playUrl = url
        playButton.isHidden = false
        //playButton.isSelected = false
        if playbackObserver != nil {
            player?.removeTimeObserver(playbackObserver!)
            playbackObserver = nil
        }
        if player?.rate == 1 {
            player?.pause()
        }
        playerLayer?.removeFromSuperlayer()
        playerSlider.removeTarget(self, action: #selector(changePlayerProgress), for: .valueChanged)
        playerSlider.value = 0.0
        timeLabel.text = "00:00/00:00"
    }
    
    //MARK:- change player progress
    @objc fileprivate func changePlayerProgress() {
        let seekDuration = playerSlider.value
        player?.seek(to: CMTimeMake(value: Int64(seekDuration), timescale: 1), completionHandler: { (BOOL) in
        })
    }

    //MARK:- setting player display
    @objc func startPlay() {
        if let url = MergingController.shared.getSavedVideoPath(fileName: "newVideo.mp4") as String?, url != ""{
            playUrl = url
            DispatchQueue.main.async {
                if self.player == nil {
                    self.setPlayRemoteUrl()
                }
                
                if self.player?.rate == 0 {
                    self.player?.play()
                    self.playButton.isSelected = true
                    
                } else {
                    self.player?.pause()
                    self.playButton.isSelected = false
                }
            }
        }else{
           MergingController.shared.downloadAudioFile()
        }
    }
    
    //MARK:- setting player
    fileprivate func setPlayRemoteUrl() {
        if playUrl == nil || playUrl == "" {
            return
        }
        removeAllObserver()
        resettingObject()
        let asset = AVAsset(url: URL.init(fileURLWithPath: playUrl))
        playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspect
        playerLayer?.contentsScale = UIScreen.main.scale
        playerLayer?.frame = self.bounds
        layer.insertSublayer(playerLayer!, at: 0)
        setAllObserver()
    }
    
    //MARK:- setting observer
    fileprivate func setAllObserver() {
        player?.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)
        playerItem?.addObserver(self, forKeyPath: "loadedTimeRanges", options: NSKeyValueObservingOptions.new, context: nil)
        playerItem?.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    fileprivate func removeAllObserver() {
        player?.removeObserver(self, forKeyPath: "rate")
        playerItem?.removeObserver(self, forKeyPath: "loadedTimeRanges")
        playerItem?.removeObserver(self, forKeyPath: "status")
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "status" {
            observePlayerStatus()
            
        } else if keyPath == "rate" {
            if (object as! AVPlayer).rate == 0 && Int(playerSlider.value) == Int(playerSlider.maximumValue)  {
                playerLayer?.removeFromSuperlayer()
                playerSlider.maximumValue = 0
                timeLabel.text = "00:00/00:00"
                self.playButton.isSelected = false
                self.player = nil
            }
        }
    }
    
    //MARK:- check playItem status
    fileprivate func observePlayerStatus() {
        
        let status:AVPlayerItem.Status = (player?.currentItem?.status)!
        switch status {
        case .readyToPlay:
            if Float(CMTimeGetSeconds((playerItem?.duration)!)).isNaN  == true {
                break
            }
            playerSlider.addTarget(self, action: #selector(changePlayerProgress), for: .valueChanged)
            playerSlider.maximumValue = Float(CMTimeGetSeconds((playerItem?.duration)!))
            let allTimeString = timeFotmatter(Float(CMTimeGetSeconds((playerItem?.duration)!)))
            playbackObserver = player?.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 1), queue: nil, using: { (time) in
              
                guard let during = self.playerItem?.currentTime() else{
                    return
                }
                
                let time = during.value / Int64(during.timescale)
                self.timeLabel.text = "\(self.timeFotmatter(Float(time))) / \(allTimeString)"
                if !self.playerSlider.isHighlighted {
                    self.playerSlider.value = Float(time)
                }
            })
            break
        case .failed:
            
            break
        default:
            break
        }
    }
    
    //MARK:- get buffer time duration
    fileprivate func getBufferTimeDuration() -> TimeInterval {
        let loadedTimeRanges =  player!.currentItem!.loadedTimeRanges
        guard let timeRange = loadedTimeRanges.first?.timeRangeValue else { return 0.0 }
        let start = CMTimeGetSeconds(timeRange.start)
        let duration = CMTimeGetSeconds(timeRange.duration)
        let currentTimeDuration = (start + duration)
        return currentTimeDuration
    }
    
    //MARK:- calculate time formatter
    fileprivate func timeFotmatter(_ time:Float) -> String {
        var hr:Int!
        var min:Int!
        var sec:Int!
        var timeString:String!
        if time >= 3600 {
            hr = Int(time / 3600)
            min = Int(time.truncatingRemainder(dividingBy: 3600))
            sec = Int(min % 60)
            timeString = String(format: "%02d:%02d:%02d", hr, min, sec)
        } else if time >= 60 && time < 3600 {
            min = Int(time / 60)
            sec = Int(time.truncatingRemainder(dividingBy: 60))
            timeString = String(format: "%02d:%02d", min, sec)
        } else if time < 60 {
            sec = Int(time)
            timeString = String(format: "00:%02d", sec)
        }
        return timeString
    }
    
    //MARK: - resetting display view
    fileprivate func resettingObject() {
        player?.pause()
        player = nil
        playerLayer = nil
        playbackObserver = nil
        playerItem = nil
    }
    
    @objc public func stopVideo(){
       self.removeAllObserver()
       self.resettingObject()
    }
    
    public func startPlayback() {
        player?.play()
    }
    
    public func pausePlayback() {
        player?.pause()
    }
    
}
