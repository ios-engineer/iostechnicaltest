
import UIKit

class CustomSlider: UISlider {
 
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        let rect = super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
        return CGRect(x: rect.origin.x, y: rect.origin.y + 2, width: rect.width, height: rect.height)
    }
    
}
